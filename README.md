# Air Quality Node
Air pollution monitoring is vital not only to citizens, warning them on the health risks of air pollutants, but also to policy-makers, assisting them on drafting regulations and laws that aim at minimizing those health risks. These stations produce only aggregated information about air pollutants, and are unable to capture variations in individual's air pollution exposure. In this project, we will make a Air Quality Monitor Node that measures atmospheric temperature, humidity, air quality parameters like PM 1.0,PM 2.5, PM 10 and CO using the controller board Arduino Mega 2560 integrate with RFM 95 chip. The interfacing sensors are used to collect the various data mentioned above and the board's RF module used to send the data to the nearest gateway and this data is pushed into the influx database which saves the data for data acquisition! .In this process, we create a software sketch that will run the Air Quality Monitor Station.And also we can visualize the data with the help of Grafana software. 

# Key Features
- BME 280 :-
 - Sensing Humidity,pressure,temperature,altitude
 - Communication protocol- Works in both SPI & I2c
 - Operating voltage-3.3v-5v
 - Operating range - temperature(-40-85*c),humidity(0-100%),
 - Pressure(300-1100 hpa)
 - Current Consumption - 3.6 uA
- MQ 135 :-
 - Air quality gas sensor module 
 - Sensitive material is Tin oxide 
 - High Sensitivity to ammonia ,sulfide,benzene,CO
 - Detection Range - 10 to 1000 ppm
 - Operating voltage - 5v 
 - Power Consumption-150 mA
 - Operating Temperature - (-10*c to 45*c)
 - Works in Dual signal output(both digital & analog)
- PMS 7003(Particulate matter sensor) :-
 - Particle Concentration sensor
 - Working is based on the principle of Laser scattering
 - Sensing particle size (PM 1.0,PM2.5,PM10 etc),temperature,humidity
 - Power Consumption - 25-50 mA
 - Operating Voltage-5V
 - Particle sensing range-0-500 ug /m^3
 - Resolution - 1 ug /m^3
- RFM 95 Module:-
 - Module used to communicate with Chirp stack server
 - Programmable bit rate up to 300 kbps.
 - Operating voltage - 3.3v
 - High sensitivity: down to -148 dBm
 - Provides ultra-long range spread spectrum communication and 
   high interference immunity whilst minimising current consumption
 - Transmits messages between (865 - 867Mhz) frequency range in india.
- Arduino Mega 2560:-
 - Operating voltage-5V
 - Digital i/o pins-54 pins
 - Storage -32768 bits
 - PWM pins -15
 - Analog pins - 16
 - Serial Ports -4
 - Supports SPI & I2C
 - Flash memory - 256 kB 

# Getting Started

- Install Arduino IDE.
- Integrate RFM 95 Module with mega 2560
- Download required libraries

# Prerequisites
- Arduino IDE 1.8.9[Tested]
- Arduino Mega 2560 
- MQ 135 Sensor 
- BME 280 Sensor 
- PMS 7003 Sensor 
- RFM 95 Module
- Selected library-MCCI lmic library,PMserial master,Adafruit_BME280_Library,
Adafruit_BusIO, Adafruit_Unified Sensor
# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Troubleshooting

# Acknowledgments

# Contribute to development

# Changelog

